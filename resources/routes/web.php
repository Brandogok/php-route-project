<?php
require_once __DIR__ . '/../../app/core/Route.php';

use \app\core\Route;


// Route::get('/', function () {
//     view('home');
// });

// Route::get('/users/{id}/works/{work_id}', function () {
// })->where([
//     'id' => '([0-9]+)',
//     'work_id' => '([0-9a-z]+)'
// ]);s

// Route::post('/register1', function () {
// });

// Route::get('/register', function () {
//     echo 'register sayfasi';
// });

Route::get('/', 'HomeController@index');
Route::get('/users', 'UsersController@index');
Route::post('/create', 'UsersController@addUser');
Route::get('/create', 'UsersController@createUserForm');
Route::get('/update', 'UsersController@update');
Route::get('/login', 'AdminLoginController@loginShowForm');
Route::post('/login', 'AdminLoginController@login');
Route::get('/logout', 'AdminLoginController@logout');

Route::prefix('/admin')->group(function () {

    Route::get('/', 'AdminController@index');
    Route::get('/login', 'AdminController@login');
});


Route::dispatch();
