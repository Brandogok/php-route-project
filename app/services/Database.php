<?php

class Database
{
    public  $connection;
    private $host = 'db';
    private $db = 'db';
    private $username = 'db';
    private $password = 'db';

    public function connect()
    {
        try {

            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db", $this->username, $this->password);
        } catch (PDOException $w) {
            return $w->getMessage();
        }
    }
}