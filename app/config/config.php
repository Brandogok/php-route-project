<?php
define('CONTROLLERS_PATH', 'app/controllers');
define('VIEWS_PATH', 'app/views');
define('MODELS_PATH', 'app/models');
define('CSS_PATH', '/public/assets/css');
define('SITE_URL', 'https://php-router.ddev.site');
define('JS_PATH', '/public/js');
define('IMG_PATH', '/public/img');
define('VENDOR_PATH', '/public/vendor');
