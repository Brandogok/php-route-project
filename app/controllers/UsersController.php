<?php

namespace app\controllers;

use app\models\UserModel;
use app\repository\UserRepository;

class UsersController
{
    public function index()
    {
        $userRepository = new UserRepository();
        $data['users'] = $userRepository->getAllUsers();
        view('includes/users', $data);
    }

    public function  createUserForm()
    {
        view('includes/create');
    }

    public function addUser()
    {
        $user = new UserModel;
        $user->setFirst_name($_POST['first_name']);
        $user->setEmail($_POST['email']);
        $user->setCity($_POST['city']);

        $userRepository = new UserRepository();
        $user = $userRepository->insert($user);

        if ($user) {
            header('Location: /', 201);
            exit;
        }

        header('Location: /create', 500, $user);
        exit;
    }

    public function update()
    {
        $userRepository = new UserRepository();
        $data['users'] = $userRepository->getAllUsers();
        view('includes/update', $data);
    }
}
