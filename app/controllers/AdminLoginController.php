<?php

namespace app\controllers;

use app\core\Controller;
use app\models\AdminModel;
use Exception;
use PDO;

class AdminLoginController extends Controller
{
    public function loginShowForm()
    {
        view('includes/login');
    }

    public function login()
    {

        $login = new AdminModel();

        $email = $_POST['email'];
        $password = $_POST['password'];

        $control = $login->adminsLogin($email, md5($password));

        if ($control['status']) {

            header('location: /');
        } else {

            header('location: /login');
        }

        view('includes/login');
    }

    public function logout()
    {
        $logout = new AdminModel();
        $logout->Adminslogout();
        view('includes/login');
    }
}
