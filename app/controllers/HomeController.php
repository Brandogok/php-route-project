<?php

namespace app\controllers;

use Database;

class HomeController
{
    public function index(): void
    {
        view('home');
    }
}
