<?php

namespace app\core;

class Controller
{
    public function load_model($model)
    {
        if (file_exists('../../app/models/Usermodel.php' . $model . '.php')) {

            require_once '../../app/models/Usermodel.php' . $model . '.php';
            return $model = new $model();
        }

        return false;
    }
}
