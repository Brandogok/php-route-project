<?php

namespace app\repository;

use app\models\UserModel;
use Database;
use Exception;
use PDO;

class UserRepository extends Database
{
    private $table = 'users';
    private $db;

    public function __construct()
    {
        $this->db = $this->connect();
    }

    public function getAllUsers()
    {
        require_once '../app/models/UserModel.php';

        $result = $this->connection->prepare('SELECT * FROM users');
        $result->execute();

        $users = [];
        foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $data) {
            $user = new UserModel();
            $user->setId($data['id']);
            $user->setFirst_name($data['first_name']);
            $user->setEmail($data['email']);
            $user->setCity($data['city']);

            $users[] = $user;
        }

        return $users;
    }

    public function getUserByEmail($email): UserModel
    {
        require_once '../app/models/UserModel.php';

        $result = $this->connection->prepare("SELECT * FROM users WHERE email = '" . $email . "'");
        $result->execute();

        $data = $result->fetchAll()[0];
        $user = new UserModel();
        $user->setId($data['id']);
        $user->setFirst_name($data['first_name']);
        $user->setEmail($data['email']);
        $user->setCity($data['city']);

        return $user;
    }

    public function insert($user): UserModel|false
    {
        require_once '../app/models/UserModel.php';

        $sql = $this->connection->prepare('INSERT INTO users (first_name, email, city) VALUES (?,?,?)');
        $result = $sql->execute([
            $user->getFirst_name(),
            $user->getEmail(),
            $user->getCity(),
        ]);

        if ($result) {
            $user = $this->getUserByEmail($user->getEmail());
            return $user;
        }

        return false;
    }
}
