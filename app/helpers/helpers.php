<?php

if (!function_exists('view')) {

    function view(string $filePath, array $data = [])
    {
        $filePath = dirname($_SERVER['DOCUMENT_ROOT']) . '/html/app/views/' . $filePath . '.php';

        if (file_exists($filePath)) {

            return require_once $filePath;
        }

        return require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/html/app/views/errors/404.php';
    }
}

if (!function_exists('request')) {

    function request(): array
    {
        return $_REQUEST;
    }
}
