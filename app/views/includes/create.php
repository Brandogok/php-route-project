<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<div class="container" style="margin-top: 150px;">
    <div class="d-grid gap-2 d-md-flex mb-4">
        <a href="/"><button class="btn btn-primary">Home</button></a>
    </div>
    <div class="d-grid gap-2 d-md-flex mb-4 justify-content-md-end">
        <a href="/users"><button class="btn btn-primary">Benutzer</button></a>
    </div>

    <div class="container">
        <form action="/create" method="post">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Vorname</label>
                <input type="text" name="first_name" class="form-control" id="user_first_name"
                    aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email adresse</label>
                <input type="email" name="email" class="form-control" id="user_email" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Ort</label>
                <input type="text" name="city" class="form-control" id="user_city" aria-describedby="emailHelp">
            </div>
            <button type="submit" class="btn btn-primary">Benutzer Hinzufügen</button>
        </form>
    </div>

</div>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</body>

</html>