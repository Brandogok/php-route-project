<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<div class="container" style="margin-top: 150px;">
    <div class="d-grid gap-2 d-md-flex mb-4">
        <a href="/"><button class="btn btn-primary">Home</button></a>
    </div>
    <div class="d-grid gap-2 d-md-flex mb-4 justify-content-md-end">
        <a href="/create"><button class="btn btn-primary">Benutzer Hinzufügen</button></a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Vorname</th>
                <th scope="col">E-mail</th>
                <th scope="col">Ort</th>
                <th scope="col">Bearbeitung</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['users'] as $user) : ?>
                <tr>
                    <th scope="row"><?= $user->getId(); ?></th>
                    <td><?= $user->getFirst_name(); ?></td>
                    <td><?= $user->getEmail(); ?></td>
                    <td><?= $user->getCity(); ?></td>
                    <td scope="col">
                        <a href="/update">
                            <button class="btn btn-primary">Aktualisieren</button>
                        </a>
                    </td>
                    <td scope="col">
                        <a href="/delete/<?= $user->getId() ?>">
                            <button class="btn btn-primary">Löschen</button>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>



</div>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</body>

</html>