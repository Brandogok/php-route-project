<?php
session_start();
// include_once(VIEWS_PATH . 'inc/header.php');
include_once('../app/views/includes/header.php');
?>


<div class="container">
    <p class="deneme">home</p>
</div>

<div class="container d-flex">
    <a href="/users"><button class="btn btn-primary">Users</button></a>
</div>

<div class="container d-flex justify-content-end">
    <a href="/logout"><button class="btn btn-primary">Logout</button></a>
</div>

<?php
include_once('../app/views/includes/footer.php');
?>