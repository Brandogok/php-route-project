<?php

namespace app\models;

use app\core\Model;
use Exception;
use PDO;

class AdminModel extends Model
{
    private $db;

    public function __construct()
    {
        $this->db = $this->connect();
    }

    public function adminsLogin($email, $password)
    {

        try {

            $query  = $this->connection->prepare('SELECT * FROM admins WHERE email = ? and password = ? ');
            $query->execute([
                $email,
                $password
            ]);

            if ($query->rowCount() == 1) {

                $row = $query->fetch(PDO::FETCH_ASSOC);

                $_SESSION['admins']  = [

                    'email' => $email,
                    'password' => $password,
                    'first_name' => $row['first_name'],
                ];


                return ['status' => true];
                header('location:/');
            } else {

                return ['status' => false];
                header('location:/login');
            }
        } catch (Exception $e) {
            return ['status' => true, 'error' => $e->getMessage()];
        }
    }

    public function Adminslogout()
    {
        unset($_SESSION['admins']);
        header('location:/login');
        exit;
    }
}
