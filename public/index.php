<?php
// ini_set('session.gc_maxlifetime', 3600);
// session_set_cookie_params(3600);

require_once '../app/config/config.php';
require_once './../app/services/Database.php';
require_once './../app/core/Model.php';
require_once './../app/helpers/helpers.php';
require_once './../app/models/UserModel.php';
require_once './../app/repository/UserRepository.php';
require_once './../app/models/AdminModel.php';
require_once './../app/core/Controller.php';
require_once './../resources/routes/web.php';
